/**
 * Created by phangty on 26/9/16.
 */
// Load required packages
// 1. import the  mongoose model
var Todo = require('../models/todo');
// 2. this is for timezone topic
var moment = require('moment-timezone');

// Create endpoint /api/todos for POST
exports.postTodos = function(req, res) {
    var todo = new Todo();

    // Set the todo properties that came from the POST data
    todo.order = req.body.order;
    todo.title = req.body.title;
    todo.completed = req.body.completed;
    todo.userId = req.user._id;

    todo.save(function(err) {
        if (err)
            return res.send(err);

        res.json({ message: 'todo added!', data: todo });
    });
};

// Create endpoint /api/todos for GET
exports.getTodos = function(req, res) {
    var timeZone = req.headers['x-timezone'];

    // filtering title
    if(req.query.title){
        Todo.findOne({ title: req.query.title }, function(err, todo) {
            if (err)
                return res.send(err);

            // Timezone conversion
            if(timeZone){
                var TzConvertedTodo = {};
                console.log(todo.dueDate.toString());
                var dueDateTz = moment.tz(todo.dueDate, timeZone);
                var updatedDateTz = moment.tz(todo.updatedAt, timeZone);
                var createdDateTz = moment.tz(todo.createdAt, timeZone);
                var completionDateTz = moment.tz(todo.completionDate, timeZone);
                console.log(dueDateTz.toString());
                TzConvertedTodo._id = todo._id;
                TzConvertedTodo.updatedAt = updatedDateTz;
                TzConvertedTodo.createdAt = createdDateTz;
                TzConvertedTodo.dueDate = dueDateTz;
                TzConvertedTodo.order = todo.order;
                TzConvertedTodo.title = todo.title;
                TzConvertedTodo.completed = todo.completed;
                TzConvertedTodo.userId = todo.userId;
                TzConvertedTodo.completionDate = completionDateTz;
                console.log(TzConvertedTodo.dueDate.toString());
                res.json(TzConvertedTodo);
            }else{
                res.json(todo);
            }
        });
    }else{
        // rest api pagination
        if(req.query.offset &&  req.query.limit) {
            Todo.paginate({userId: req.user._id}, { offset: parseInt(req.query.offset), limit: parseInt(req.query.limit) }, function(err, result) {
                console.log(result);
                res.json(result);
            });
        }else{
            Todo.find({ userId: req.user._id }, function(err, todos) {
                if (err)
                    return res.send(err);
                res.json(todos);
            });
        }

    }
};

// Create endpoint /api/todos/:todo_id for GET
exports.getTodo = function(req, res) {
    Todo.find({ userId: req.user._id, _id: req.params.todo_id }, function(err, todo) {
        if (err)
            return res.send(err);

        res.json(todo);
    });
};

// Create endpoint /api/todos/:todo_id for PUT
exports.putTodo = function(req, res) {
    Todo.update({ userId: req.user._id, _id: req.params.todo_id }, { completed: req.body.completed }, function(err, num, raw) {
        if (err)
            return res.send(err);

        res.json({ message: num + ' updated' });
    });
};

// Create endpoint /api/todos/:todo_id for DELETE
exports.deleteTodo = function(req, res) {
    Todo.remove({ userId: req.user._id, _id: req.params.todo_id }, function(err) {
        if (err)
            return res.send(err);

        res.json({ message: 'Todo removed!' });
    });
};